package model;

import java.util.List;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class Vehicle {

    private final String type;
    private final String brand;
    private final String model;
    private final String color;
    private final String licensePlate;
    private final String chassis;
    private final String VehicleRegistration; // National Vehicle Registration // Renavam(Brazil).
    private List<Debt> debts;

    public Vehicle(String type, String brand, String model, String color, String licensePlate, String chassis, String VehicleRegistration) {
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.licensePlate = licensePlate;
        this.chassis = chassis;
        this.VehicleRegistration = VehicleRegistration;
    }

    public List<Debt> getDebts() {
        return debts;
    }

    public void setDebts(List<Debt> debts) {
        this.debts = debts;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getChassis() {
        return chassis;
    }

    public String getVehicleRegistration() {
        return VehicleRegistration;
    }

    @Override
    public String toString() {
        return "Veículo" + "\nTipo: " + type + "\nMarca: " + brand + "\nModelo: " + model + "\nCor: " + color + "\nPlaca: " + licensePlate
                + "\nChassi: " + chassis + "\nRenavam: " + VehicleRegistration + "\nDébitos:" + debts;
    }
}
