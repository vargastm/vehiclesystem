package model;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class Debt {

    private String name;
    private String expiration;
    private Double value;

    public Debt(String name, String expiration, Double value) {
        this.name = name;
        this.expiration = expiration;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "\nNome: " + name + " | Vencimento: " + expiration + " | Valor: " + value;
    }

}
