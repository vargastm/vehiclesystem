package view;

import controller.VehicleControl;
import java.util.List;
import model.Debt;
import model.Vehicle;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ViewVehicle {

    public static void main(String[] args) {
        VehicleControl control = new VehicleControl();
        List<Vehicle> vehicles;
        Vehicle vehicle;
        
        vehicles = control.getVehicles();
        System.out.println(vehicles.toString());
    }
}
