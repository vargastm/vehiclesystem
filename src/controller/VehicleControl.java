package controller;

import java.util.ArrayList;
import java.util.List;
import model.Debt;
import model.Vehicle;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class VehicleControl {

    private List<Debt> debts = new ArrayList<>();
    private List<Vehicle> vehicles = new ArrayList<>();
    
    public List<Vehicle> getVehicles(){
        Vehicle v1 = new Vehicle("Carro", "Fiat", "Uno", "Azul", "UNO-1111", "9BW ZZZ377 VT 004251", "28618644210");
        v1.setDebts(getDebts());
        vehicles.add(v1);
        return vehicles;
    }

    public List<Debt> getDebts() {
        Debt ipva = new Debt("IPVA", "11/09/2019", 99.00);
        Debt insurance = new Debt("Seguro Obrigatorio", "21/09/2019", 199.00);
        Debt licensing = new Debt("Licenciamento", "31/09/2019", 299.00);
        debts.add(ipva);
        debts.add(insurance);
        debts.add(licensing);
        return debts;
    }
}
